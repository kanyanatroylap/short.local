<?php


Route::get('/','TodosController@index');
Route::get('/t/{short}', 'TodosController@show');

//\Illuminate\Support\Facades\Route::resource('/','TodosController');

Route::resource('/','TodosController');
