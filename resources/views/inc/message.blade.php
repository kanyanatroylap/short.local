@if(session('success'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        {{ session('success') }}

        <button type="button" class="close" data-dismiss="alert">
            <span>X</span>
        </button>
    </div>
@endif


