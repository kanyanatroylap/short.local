
@extends('layouts.app')
@section('content')
    <h1 class="mt-3">List</h1>
    @if(count($todos)>0)
        @foreach($todos as $todo)
            <div>
                <p>{{$todo->created_at}}</p>
                <a href="{{url($todo->long_url)}}">
                    <h3 class="text-warning">{{$todo->long_url}}</h3>
                </a>
                <input id="todo{{$todo->id}}" class="form-control" type="text" value="http://www.short.local/t/{{$todo->short_url}}" readonly>
                <button onclick="copy(this)" value="{{$todo->id}}" type="button" class="btn btn-info">copy</button>
                <p>{{$todo->view}}</p>
            </div>
            <hr>
        @endforeach
    @endif
    <script>
        function copy(clickedBtn) {
            var id = clickedBtn.value;
            var copyText = document.querySelector('#todo'+id);
            copyText.select();
            document.execCommand('copy');
            alert('Copied' + copyText.value);

        }
    </script>
@endsection
