{{--create.blade.php--}}
@extends('layouts.app')
@section('content')
    <h1>Shorten URL</h1>
    <form method="post" action="{{ url('/')}}">
        @csrf
        <div class="form-group">
            <label>Long URL</label>
            <input type="text" name="long_url" class="form-control">
        </div>
        <button type="submit" class="btn btn-info" >CREATE SHORT URL</button>
    </form>
@endsection
