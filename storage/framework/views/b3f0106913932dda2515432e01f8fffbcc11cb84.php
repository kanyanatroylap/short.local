<?php $__env->startSection('content'); ?>
    <h1>Shorten URL</h1>
    <form method="post" action="<?php echo e(url('/')); ?>">
        <?php echo csrf_field(); ?>
        <div class="form-group">
            <label>Long URL</label>
            <input type="text" name="long_url" class="form-control">
        </div>
        <button type="submit" class="btn btn-info" >CREATE SHORT URL</button>
    </form>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/short/resources/views/create.blade.php ENDPATH**/ ?>